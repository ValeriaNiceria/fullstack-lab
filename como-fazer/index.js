const express = require('express')
const app = express()
const axios = require('axios')

app.set('view engine', 'ejs')

const port = process.env.PORT || 3000

// let aux = 10;
// const resolver = (request, response) => {
//     aux++;
//     response.send(`<h1>Olá!!! => ${aux}</h1>`)
// }
// app.get('/', resolver)

// let aux = 10
// app.get('/', (request, response) => {
//     aux++
//     response.render('index', { aux: aux })
// })


app.get('/', async(request, response) => {
    const content = await axios.get('https://como-fazer-5a4a0.firebaseio.com/teste.json')
    console.log(content.data)
    response.render('index', { aux: content.data })
})


app.listen(port, (error) => {
    if (error) {
        console.log('error')
    } else {
        console.log('Como fazer um servidor rodar na porta:', port)
    }
})
